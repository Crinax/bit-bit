mod domain;
mod test;

use domain::{BitArray, Bit};


fn main() {
    println!("{}", BitArray::from(0b00101));
    let d: Vec<u8> = BitArray::from(0b00101).into();

    println!("{:?}", d);
}
