use std::{
    fmt::Display,
    ops::{Add, BitXor},
};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct BitArray(Vec<bool>);

impl BitArray {
    pub fn new() -> Self {
        Self(vec![])
    }
}

macro_rules! impl_from_bitarray {
    ($($T: ty),*) => {
        $(
            impl From<$T> for BitArray {
                fn from(value: $T) -> Self {
                    Self(
                        format!("{:b}", value)
                            .chars()
                            .into_iter()
                            .map(|x| match x {
                                '0' => false,
                                _ => true,
                            })
                            .collect::<Vec<_>>()
                    )
                }
            }
        )*
    };
}

macro_rules! impl_into_vec_bitarray {
    ($($T: ty),*) => {
        $(
            impl Into<Vec<$T>> for BitArray {
                fn into(self) -> Vec<$T> {
                    self
                        .0
                        .into_iter()
                        .map(|x| match x {
                            false => 0,
                            true => 1,
                        })
                        .collect::<Vec<_>>()
                }
            }
            )*
    };
}

impl Into<Vec<Bit>> for BitArray {
    fn into(self) -> Vec<Bit> {
        return self
            .0
            .into_iter()
            .map(|x| Bit::new(x))
            .collect()
    }
}

impl Display for BitArray {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let result = self
            .0
            .iter()
            .map(|x| match x {
                false => "0",
                true => "1",
            })
            .collect::<Vec<_>>()
            .join("");

        write!(f, "{}", result)
    }
}

impl_from_bitarray!(u8, u16, u32, u64, u128, i8, i16, i32, i64, i128);
impl_into_vec_bitarray!(u8, u16, u32, u64, u128, i8, i16, i32, i64, i128);

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Bit(bool);

impl Bit {
    pub fn new(value: bool) -> Self {
        Self(value)
    }
}

impl BitXor for Bit {
    type Output = Self;

    fn bitxor(self, rhs: Self) -> Self::Output {
        Self(self.0 ^ rhs.0)
    }
}

impl Add for Bit {
    type Output = BitArray;

    fn add(self, rhs: Self) -> Self::Output {
        if self.0 {
            if rhs.0 {
                return BitArray::from(0b10);
            }

            return BitArray::from(0b1);
        }

        if rhs.0 {
            return BitArray::from(0b1);
        }

        return BitArray::from(0b0);
    }
}

impl Display for Bit {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let result = match self.0 {
            true => 1,
            false => 0,
        };

        write!(f, "{}", result)
    }
}

macro_rules! generate_from {
    ($($T:ty),*) => {
        $(
            impl From<$T> for Bit {
                fn from(value: $T) -> Self {
                    match value {
                        0 => Self(false),
                        _ => Self(true),
                    }
                }
            }
        )*
    };
}

macro_rules! generate_into {
    ($($T:ty),*) => {
        $(
            impl Into<$T> for Bit {
                fn into(self) -> $T {
                    match self.0 {
                        true => 1,
                        false => 0,
                    }
                }
            }
        )*
    };
}

generate_from!(u8, u16, u32, u64, u128, i8, i16, i32, i64, i128);
generate_into!(u8, u16, u32, u64, u128, i8, i16, i32, i64, i128);
