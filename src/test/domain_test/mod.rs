#[test]
fn from_unsigned() {
    use crate::domain::Bit;

    let bit_array = [
        Bit::from(0u8),
        Bit::from(1u8),
        Bit::from(0u16),
        Bit::from(1u16),
        Bit::from(0u32),
        Bit::from(1u32),
        Bit::from(0u64),
        Bit::from(1u64),
        Bit::from(0u128),
        Bit::from(1u128),
    ];

    let bit_bool_array = [
        Bit::new(false),
        Bit::new(true),
        Bit::new(false),
        Bit::new(true),
        Bit::new(false),
        Bit::new(true),
        Bit::new(false),
        Bit::new(true),
        Bit::new(false),
        Bit::new(true),
    ];

    assert_eq!(bit_array, bit_bool_array);
}
